# Spring Boot Excel

### Things todo list

1. Clone this repository: `https://gitlab.com/hendisantika/SpringBootExcel.git`
2. Navigate to the folder: `cd SpringBootExcel`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080/excel
